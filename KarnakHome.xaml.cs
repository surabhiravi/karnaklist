﻿using System.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
//using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for KarnakHome.xaml
    /// </summary>
    public partial class KarnakHome : Page
    {
        string lastPart;
        string result;
        public KarnakHome()
        {
            
                InitializeComponent();
            
        }
         
        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // string item = comboBox.SelectedItem.ToString();
            //MessageBox.Show(item);
            //ComboBoxItem ComboItem = (ComboBoxItem)comboBox.SelectedItem;
            //string name = ComboItem.Name;
            // string name = comboBox.SelectionBoxItem.ToString();
            //string s = comboBox.Items.GetItemAt(comboBox.SelectedIndex).ToString();
            string s = comboBox.SelectedValue.ToString();
           // MessageBox.Show(s);
             lastPart = s.Split(':').Last();
           result = lastPart.Trim();
           // MessageBox.Show(result);
            //return lastPart;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            DeviceList devlist = new DeviceList(result);
            this.NavigationService.Navigate(devlist);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Add_device adddev = new Add_device();
            this.NavigationService.Navigate(adddev);
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
           ViewAll  view = new ViewAll();
            this.NavigationService.Navigate(view);
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            ImportExcelPage excellist = new ImportExcelPage();
            this.NavigationService.Navigate(excellist);
        }
    }
}
