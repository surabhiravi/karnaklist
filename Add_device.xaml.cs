﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SQLite;   
using System.Data;
//using Finisar.SQLite;
namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for Add_device.xaml
    /// </summary>
    public partial class Add_device : Page
    {
         string dbConnectionString = @"Data Source=database.db;Version=3;New=True;Compress=True;";
       
        public Add_device()
        {
            InitializeComponent();
           
        }

        

        //SqlConnection conn = new SqlConnection(@"Data Source=.\(LocalDB)\\MSSQLLocalDB;AttachDbFilename=
        //.\C:\\Users\\surabhi.ravi\\documents\\visual studio 2015\\Projects\\WpfApplication1\\Database1.mdf;Integrated Security=True");
        private void button_Click(object sender, RoutedEventArgs e)
        {
           // MessageBox.Show("inside add");
            string version = this.version.Text;
            string code = this.code.Text;
            string model = this.model.Text;
            //MessageBox.Show(code);
            SQLiteConnection sqlite_conn = new SQLiteConnection(dbConnectionString);
            SQLiteCommand sqlite_cmd;
           // SQLiteDataReader sqlite_datareader;
            //MessageBox.Show("db created ");
            if (this.version.Text != "" || this.code.Text != "" || this.model.Text != "")
            {
                try
                {


                    // open the connection:
                    sqlite_conn.Open();

                    // create a new SQL command:
                    sqlite_cmd = sqlite_conn.CreateCommand();

                    // Let the SQLiteCommand object know our SQL-Query:
                    // sqlite_cmd.CommandText = "CREATE TABLE device (version varchar(100) primary key, codeName varchar(100),modelName varchar(100));";

                    // Now lets execute the SQL ;D
                    // sqlite_cmd.ExecuteNonQuery();

                    // Lets insert something into our new table:
                    sqlite_cmd.CommandText = "INSERT INTO device (version,codeName,modelName,predecessor) VALUES (' " + this.version.Text + " ', ' " + this.code.Text + "',' " + this.model.Text + " ',' " + this.predecessor.Text + " ');";

                    // And execute this again ;D
                    int i = sqlite_cmd.ExecuteNonQuery();
                    if (i == 1)
                    {
                        MessageBox.Show("Successfully Created!");
                        this.version.Text = "";
                        this.code.Text = "";
                        this.model.Text = "";
                        this.predecessor.Text = "";
                    }

                    // But how do we read something out of our table ?
                    // First lets build a SQL-Query again:
                   
                    sqlite_conn.Close();
                }
                catch (Exception ee) { }
            }
            else
            {
                MessageBox.Show("Required Field!");
            }


        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            KarnakHome home = new KarnakHome();
            this.NavigationService.Navigate(home);
        }
    }
}
