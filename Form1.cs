﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Data;
namespace WpfApplication1
{
    public partial class Form1 : Form
    {
        string dbConnectionString = @"Data Source=database.db;Version=3;New=True;Compress=True;";

        public Form1()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            SQLiteConnection sqlite_conn = new SQLiteConnection(dbConnectionString);
            SQLiteCommand sqlite_cmd;
            // SQLiteDataReader sqlite_datareader;
            //MessageBox.Show("db created ");
            if (textBox1.Text != "" || textBox2.Text != "" || textBox3.Text != "")
            {
                try
                {


                    // open the connection:
                    sqlite_conn.Open();

                    // create a new SQL command:
                    sqlite_cmd = sqlite_conn.CreateCommand();

                    // Let the SQLiteCommand object know our SQL-Query:
                    // sqlite_cmd.CommandText = "CREATE TABLE device (version varchar(100) primary key, codeName varchar(100),modelName varchar(100));";

                    // Now lets execute the SQL ;D
                    // sqlite_cmd.ExecuteNonQuery();

                    // Lets insert something into our new table:
                    sqlite_cmd.CommandText = "INSERT INTO device (version,codeName,modelName,predecessor) VALUES (' " + textBox1.Text + " ', ' " + textBox2.Text + "',' " + textBox3.Text + " ',' " + textBox4.Text + " '); ";

                    // And execute this again ;D
                    int i = sqlite_cmd.ExecuteNonQuery();
                    if (i == 1)
                    {
                        MessageBox.Show("Successfully Created!");
                        textBox1.Text = "";
                        textBox2.Text = "";
                        textBox3.Text = "";
                        textBox4.Text = "";
                    }

                    // But how do we read something out of our table ?
                    // First lets build a SQL-Query again:

                    sqlite_conn.Close();
                }
                catch (Exception ee) { }
            }
        }
    }
}

