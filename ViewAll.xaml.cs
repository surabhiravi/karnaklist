﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SQLite;
using System.Data;
using System.ComponentModel;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for ViewAll.xaml
    /// </summary>
    public partial class ViewAll : Page
    {
        string dbConnectionString = @"Data Source=database.db;Version=3;New=True;Compress=True;";
        SQLiteConnection conn;
        SQLiteCommand cmd;
        SQLiteDataAdapter adapter;
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();

        //String connectString;
        public ViewAll()
        {
            InitializeComponent();
            ReadData();
        }

        private void ReadData()
        {
            try
            {
                conn = new SQLiteConnection(dbConnectionString);
                conn.Open();
                cmd = new SQLiteCommand();
                String sql = "SELECT * FROM device";
                adapter = new SQLiteDataAdapter(sql, conn);
                ds.Reset();
                adapter.Fill(ds);
                dt = ds.Tables[0];
                dataGrid.ItemsSource = dt.DefaultView;
                conn.Close();
           
                //dataGrid.Columns[1].Header = "Version";
                //dataGrid.Columns[2].Header = "CodeName";
                // dataGrid.Columns[3].Header = "ModelName";
                // dataGrid.Columns[0].Visibility = false;
                // dataGrid.Columns[1].Size = DataGridAutoSizeColumnMode.AllCells;
                // dataGrid.Columns[2].AutoSizeMode = DataGridAutoSizeColumnMode.AllCells;
                // dataGrid.Columns[3].AutoSizeMode = DataGridAutoSizeColumnMode.Fill;
                //dataGrid.SelectionMode = DataGridSelectionMode.FullRowSelect;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
